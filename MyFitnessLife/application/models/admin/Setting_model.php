<?php
    class Setting_model extends CI_Model{

        public function get_cur_price(){
            return $this->db->get('ci_price')->row_array();             
        }
        
        
        
        public function add_price($data) {
        
            if ($this->db->get('ci_price')->num_rows() == 0) {
                
                $this->db->insert('ci_price', $data);
                
            } else {
                
                $this->db->update('ci_price', $data);               
                
            }
            
            return true;            
        }
    }

?>