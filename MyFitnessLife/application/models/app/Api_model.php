<?php
  
  class Api_model extends CI_Model {
      
      function exist_user_email($email) {
          
          $query = $this->db->get_where('tb_user', array('email' => $email));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
      }
      
      function add_user($data) {
          
          $this->db->insert('tb_user', $data);
          return $this->db->insert_id();
      }
      
      public function login($data){
            $query = $this->db->get_where('tb_user', array('email' => $data['email']));
            if ($query->num_rows() == 0){
                return false;
            }
            else{
                //Compare the password attempt with the password we have stored.
                $result = $query->row_array();
                $validPassword = password_verify($data['password'], $result['password']);
                if($validPassword){
                    return $result = $query->row_array();
                }                
            }
      }
      
      function addNewStore($data) {
          
          $this->db->insert('tb_store', $data);
          return $this->db->insert_id();           
      }
      
      function addNewProduct($data) {
          
          $this->db->insert('tb_product', $data);
          return $this->db->insert_id();           
      }
      
      function addNewAward($data) {
          
          $this->db->insert('tb_award', $data);
          return $this->db->insert_id();           
      }
      
      function getStoreList($user_id) {
          
          $query = $this->db->get_where('tb_store', array('user_id' => $user_id));
          return $query->result_array();
      }
      
      function getProductList($store_id) {
          
          $query = $this->db->get_where('tb_product', array('store_id' => $store_id));
          return $query->result_array();
      }
      
      function getAllTeachers() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '222'));
          return $query->result_array();
      }
      
      function getAllStudents() {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333'));
          return $query->result_array();
      }
      
      function getTeacherByCreatorId($creator_id) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '222', 'creator_id' => $creator_id));
          return $query->result_array();
      }
      
      function getStudentByCreatorId($creator_id) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333', 'creator_id' => $creator_id));
          return $query->result_array();
      }
      
      function upload_photo($user_id, $data) {
        
          $this->db->where('id', $user_id); 
          $this->db->update('tb_user', $data);
          return true;
      }
      
      function getAwardList($user_id) {
          
          $query = $this->db->get_where('tb_award', array('student_id' => $user_id));
          return $query->result_array();
      }
      
      function addQrcode($user_id, $qr_code) {
          
          $this->db->where('id', $user_id);
          $this->db->set('qr_code', $qr_code);
          $this->db->update('tb_user');
      }
      
      function getStudentByQrCode($qr_code) {
          
          $query = $this->db->get_where('tb_user', array('user_type' => '333', 'qr_code' => $qr_code));
          return $query->row_array();
      } 
      
      function exist_transaction_id($transaction_id) {
          
          $query = $this->db->get_where('tb_statement', array('transaction_id' => $transaction_id));
          if ($query->num_rows() == 0){
                return false;
          } else {
                return true;
          }
          
      }     
      
      function sendMoney($data) {
          
          $this->db->insert('tb_statement', $data);
          return $this->db->insert_id();           
      }
      
      function getStatement($user_id) {
          
          $this->db->where('from', $user_id);
          $this->db->or_where('to', $user_id);
          return $this->db->get('tb_statement')->result_array();
      }
      
      function editStore($id, $data) {
          $this->db->where('id', $id);
          $this->db->update('tb_store', $data);
      }
      
      function editProduct($id, $data) {
          
          $this->db->where('id', $id);
          $this->db->update('tb_product', $data);
          
      }
      

      
  }
?>
