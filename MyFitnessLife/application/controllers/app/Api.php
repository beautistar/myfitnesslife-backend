<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include("./vendor/autoload.php");

class Api extends CI_Controller {

    

    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('app/api_model', 'api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

    private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
    }

    /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

    private function doRespondSuccess($result){

        $result['message'] = "Success.";
        $this->doRespond(200, $result);
    }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

    private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
    }
    
    function version() {
        phpinfo();
    }
    
    /**
    * Send Firebase push notification
    * 
    * @param mixed $user_id
    * @param mixed $type
    * @param mixed $body
    * @param mixed $content
    */
    function sendPush($user_id, $type, $body, $content) {
        
        // send FCM push notification
        
        $url = "https://fcm.googleapis.com/fcm/send";
        $api_key = "AAAAPSde5aY:APA91bHgm1XpjkX3HoxLToc4KgP7qEKu4cRf2f40hVnOKM02bb_-s0yfiWTMz1K9_zJNNj5VeVQ06QmsONNruudhIeokdIyMHsSpxZYHBz-cslstDXLT6dh3ETQNQu8uEHtgCg5oJM6o";
        
        $token = "";
        $token = $this->api_model->getToken($user_id);
        
        if (strlen($token) == 0 ) {

            return;
        }
        
        /*
        if(is_array($target)){
            $fields['registration_ids'] = $target;
        } else{
            $fields['to'] = $target;
        }

        // Tpoic parameter usage
        $fields = array
                    (
                        'to'  => '/topics/alerts',
                        'notification'          => $msg
                    );
        $data = array('msgType' => $type,
                      'content' => $content);
        */
        $msg = array
                (
                    'body'     => $body,
                    'title'    => 'Peek',   
                    'badge' => 1,             
                    'sound' => 'default'/*Default sound*/
                );
                
        $data = array('msgType' => "notification",
                      'content' => $content);
                      
        $fields = array
            (
                //'registration_ids'    => $tokens,
                'to'                => $token,
                'notification'      => $msg,
                'priority'          => 'high',
                'data'              => $data
            );

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        //@curl_exec($ch);
        
        $result['result'] = curl_exec($ch); 

        curl_close($ch); 
        
        return $result;
        
    }
    
    public function sendMail( $email, $code){              
             
            $to = $email;
            $subject = "Welcome to Peek.\n\n";
            $message = "Your verification code is : \n"
                        .$code.".\n\n" ;
                       
            $from = "KashIn";
            $headers = "Mime-Version:1.0\n";
            $headers .= "Content-Type : text/html;charset=UTF-8\n";
            $headers .= "From:" . $from;           
            
            return mail($to, $subject, $message, $headers);         
    }
    
    public function makeRandomCode(){
         
         mt_srand();

         $random_code = '';

         $arr = array('1','2','3','4','5','6','7','8','9','0');

         for ($i = 0 ; $i < 6 ; $i++) {

             $index = mt_rand(0, 9);
             $random_code .= $arr[$index];
         }

         return $random_code;
     }
     
    
    function stripe_payment() {
        
        $result = array();
        
        $stripe_token = $_POST['stripe_token'];
        $email = $_POST['email'];
        $amount = $_POST['amount'];
        
        try {
            
            \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

            $message = \Stripe\Charge::create(array(
              "amount" => $amount,
              "currency" => "usd",
              "source" => $stripe_token, // obtained with Stripe.js
              "description" => "Paid from ".$email
            ));
            
            $result = $message;
            $this->doRespondSuccess($result);
            
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();//`enter code here`
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $body = $e->getJsonBody();
            $err = $body['error'];
            $this->doRespond(PAYMENT_ERROR, $err);
            //return $err['message'];
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $body = $e->getJsonBody();
            $err = $body['error'];
            //return $err['message'];
            $this->doRespond(PAYMENT_ERROR, $err);
        }        
    }
    
    
    /**
    * get user object from query array result
    * 
    * @param mixed $user_array query result array
    */
    function getUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],                             
                             'name' => $user_array['name'],
                             'email' => $user_array['email'],
                             'user_type' => $user_array['user_type'],
                             'teacher_count' => $user_array['teacher_count'],
                             'student_count' => $user_array['student_count'],
                             'contract_name' => $user_array['contract_name'],
                             'qr_code' => $user_array['qr_code'],
                             'created_at' => $user_array['created_at']
                             );
        return $user_object;
    }
    
    function getCommonUser($user_array) {
        
        $user_object = array('user_id' => $user_array['id'],                             
                             'name' => $user_array['name'],
                             'email' => $user_array['email'],
                             'qr_code' => $user_array['qr_code'],
                             'user_type' => $user_array['user_type'],                              
                             'created_at' => $user_array['created_at']
                             );
        return $user_object;
    }
    
    function getStore($query_array) {
        
        $store_object = array('id' => $query_array['id'],
                              'name' => $query_array['name'],
                              'description' => $query_array['description'],
                              'image_url' => $query_array['image_url'],
                              'created_at' => $query_array['created_at']
                              );
        return $store_object;
    }
    
    function getProduct($query_array) {
        
        $object = array('id' => $query_array['id'],
                              'name' => $query_array['name'],
                              'description' => $query_array['description'],
                              'price' => $query_array['price'],
                              'image_url' => $query_array['image_url'],
                              'created_at' => $query_array['created_at']
                              );
        return $object;
    }
    
    function getAward($query_array) {
        
        $object = array('id' => $query_array['id'],
                        'name' => $query_array['name'],
                        'description' => $query_array['description'],                              
                        'image_url' => $query_array['image_url'],
                        'created_at' => $query_array['created_at']
                        );
        return $object;
    }
    

    function signup() {
        
        $result = array();
        
        $name = $_POST['name'];
        $teacher_count = isset($_POST['teacher_count']) ?  $_POST['teacher_count'] : 0;
        $student_count = isset($_POST['student_count']) ?  $_POST['student_count'] : 0; 
        $contract_name = isset($_POST['contract_name']) ?  $_POST['contract_name'] : ""; 
        $user_type = $_POST['user_type'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        if ($this->api_model->exist_user_email($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('name' => $name,
                          'teacher_count' => $teacher_count,
                          'student_count' => $student_count,
                          'contract_name' => $contract_name,
                          'email' => $email,
                          'user_type' => $user_type,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $user_id =  $this->api_model->add_user($data);
            $qr_code = $user_id.'_'.intval(microtime(true) * 10);
            $this->api_model->addQrcode($user_id, $qr_code);
            $result["user_id"] = $user_id;
            $result['qr_code'] = $qr_code;
                        
            $this->doRespondSuccess($result);  
        }
    }    
    
    function signin() {
        
        $result = array();
        
        $email = $_POST['email'];
        $password = $_POST['password'];
            
        $data = array(
            'email' => $email,
            'password' => $password
        );
        $q_result = $this->api_model->login($data);
        if ($q_result == TRUE) {
            $result['user_model'] = $this->getUser($q_result);
            $this->doRespondSuccess($result);            
        } else{
            $result['message'] = 'Invalid Email or Password!';
            $this->doRespond(202, $result);
        }        
    }
    
    function addNewStore() {
        
        $result = array();
        
        $user_id = $_POST['user_id'];
        $name = $_POST['name'];
        $description = $_POST['description'];             

                
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 3000,
            'file_name' => $name.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('name' => $name,
                          'description' => $description,                          
                          'user_id' => $user_id,                          
                          'image_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->addNewStore($data);
            $result['image_url'] = $file_url;            
            $result['id'] = $id;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }    
    
    function addNewProduct() {
        
        $result = array();
        
        $name = $_POST['name'];
        $description = $_POST['description'];
        $price = $_POST['price'];       
        $store_id = $_POST['store_id'];       

                
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 3000,
            'file_name' => $name.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('name' => $name,
                          'description' => $description, 
                          'store_id' => $store_id, 
                          'price' => $price, 
                          'image_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->addNewProduct($data);
            $result['image_url'] = $file_url;            
            $result['id'] = $id;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function addNewAward() {
        
        $result = array();
        
        $name = $_POST['name'];
        $description = $_POST['description'];
        $class = $_POST['class'];      
        $teacher_id = $_POST['teacher_id'];      
        $student_id = $_POST['student_id'];      

                  
        if(!is_dir("uploadfiles/")) {
            mkdir("uploadfiles/");
        }
        $upload_path = "uploadfiles/";  

        $cur_time = time();
         
        $dateY = date("Y", $cur_time);
        $dateM = date("m", $cur_time);
         
        if(!is_dir($upload_path."/".$dateY)){
            mkdir($upload_path."/".$dateY);
        }
        if(!is_dir($upload_path."/".$dateY."/".$dateM)){
            mkdir($upload_path."/".$dateY."/".$dateM);
        }
         
        $upload_path .= $dateY."/".$dateM."/";
        $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 3000,
            'file_name' => $name.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('image')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('name' => $name,
                          'description' => $description, 
                          'class' => $class, 
                          'teacher_id' => $teacher_id, 
                          'student_id' => $student_id, 
                          'image_url' => $file_url, 
                          'created_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->addNewAward($data);
            $result['image_url'] = $file_url;            
            $result['id'] = $id;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
    }
    
    function getStoreList() {
        
        $result = array();
        $store_result = array();
        
        $user_id = $_POST['user_id'];
        
        $qruery_array = $this->api_model->getStoreList($user_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['store_list'] = $store_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getStore($row);
                array_push($store_result, $store);
            }
            $result['store_list'] = $store_result;            
            $this->doRespondSuccess($result);            
        }        
    }
    
    function getProductList() {
        
        $result = array();
        $t_result = array();
        
        $store_id = $_POST['store_id'];
        
        $qruery_array = $this->api_model->getProductList($store_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['product_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $one = $this->getProduct($row);
                array_push($t_result, $one);
            }
            $result['product_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }        
    }
    
    function getAllTeachers() {
        
        $result = array();
        $t_result = array();
        
        
        $query_result = $this->api_model->getAllTeachers();
        
        if (count($query_result) == 0) {
            $result['message'] = "No data.";
            $result['teacher_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($query_result as $row) {
                $one = $this->getCommonUser($row);
                array_push($t_result, $one);
            }
            $result['teacher_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function getAllStudents() {
        
        $result = array();
        $t_result = array();
        
        
        $query_result = $this->api_model->getAllStudents();
        
        if (count($query_result) == 0) {
            $result['message'] = "No data.";
            $result['student_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($query_result as $row) {
                $one = $this->getCommonUser($row);
                array_push($t_result, $one);
            }
            $result['student_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function createTeacher() {
        
        $result = array();
        
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $creator_id = $_POST['creator_id'];
        
        if ($this->api_model->exist_user_email($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('name' => $name,                          
                          'email' => $email,
                          'user_type' => 222,
                          'creator_id' => $creator_id,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }
        
    }
    
    function createStudent() {
        
        $result = array();
        
        $name = $_POST['name']; 
        $email = $_POST['email'];
        $password = $_POST['password'];
        $creator_id = $_POST['creator_id'];
        
        if ($this->api_model->exist_user_email($email)) {
             $result['message'] = "Email already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('name' => $name,                          
                          'email' => $email,
                          'user_type' => 333,
                          'creator_id' => $creator_id,
                          'password' => password_hash($password, PASSWORD_BCRYPT),
                          'created_at' => date('Y-m-d h:m:s'),
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $result["user_id"] = $this->api_model->add_user($data);            
            $this->doRespondSuccess($result);  
        }        
    }
    
    function getTeacherByCreatorId() {
        
        $result = array();
        $t_result = array();
        
        $creator_id = $_POST['creator_id'];
        
        
        $query_result = $this->api_model->getTeacherByCreatorId($creator_id);
        
        if (count($query_result) == 0) {
            $result['message'] = "No data.";
            $result['teacher_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($query_result as $row) {
                $one = $this->getCommonUser($row);
                array_push($t_result, $one);
            }
            $result['teacher_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function getStudentByCreatorId() {
        
        $result = array();
        $t_result = array();
        
        $creator_id = $_POST['creator_id'];
        
        
        $query_result = $this->api_model->getStudentByCreatorId($creator_id);
        
        if (count($query_result) == 0) {
            $result['message'] = "No data.";
            $result['student_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($query_result as $row) {
                $one = $this->getCommonUser($row);
                array_push($t_result, $one);
            }
            $result['student_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function uploadPhoto() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];

                  
         if(!is_dir("uploadfiles/")) {
             mkdir("uploadfiles/");
         }
         $upload_path = "uploadfiles/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 3000,
            'max_height' => 4000,
            'file_name' => $user_id.'_'.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('photo')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $data = array('photo_url' => $file_url, 
                          'updated_at' => date('Y-m-d h:m:s')
                          );
            $id = $this->api_model->upload_photo($user_id, $data);
            $result['photo_url'] = $file_url;            
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
    }
    
    
    function getAwardList() {
        
        $result = array();
        $t_result = array();
        
        $user_id = $_POST['user_id'];
        
        $qruery_array = $this->api_model->getAwardList($user_id);
        
        if (count($qruery_array) == 0) {
            $result['message'] = "No data.";
            $result['award_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            foreach ($qruery_array as $row) {
                $store = $this->getAward($row);
                array_push($t_result, $store);
            }
            $result['award_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function getStudentByQrCode() {
        
        $result = array();
        $t_result = array();
        
        $qr_code = $_POST['qr_code'];
        
        $query_array = $this->api_model->getStudentByQrCode($qr_code);
        
        if (count($query_array) == 0) {
            $result['message'] = "User not found.";
            $result['student_information'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            $t_result = $this->getCommonUser($query_array);
            $result['student_information'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
    }
    
    function sendMoney() {
        
        $result = array();
        
        $transaction_id = $_POST['transaction_id']; 
        $from = $_POST['from'];
        $to = $_POST['to'];
        $description = $_POST['description'];
        $amount = $_POST['amount'];         
        
        if ($this->api_model->exist_transaction_id($transaction_id)) {
             $result['message'] = "Transaction already exist.";
             $this->doRespond(201, $result);             
        } else {
            $data = array('transaction_id' => $transaction_id,                          
                          'from' => $from,
                          'to' => $to,
                          'description' => $description,
                          'amount' => $amount,
                          'created_at' => date('Y-m-d h:m:s'),
                          );
            $this->api_model->sendMoney($data);            
            $this->doRespondSuccess($result);  
        }        
    }
    
    function getTransaction() {
        
        $result = array();
        $t_result = array();
        
        $user_id = $_POST['user_id'];
        
        $query_array = $this->api_model->getStatement($user_id);
        
        if (count($query_array) == 0) {
            $result['message'] = "Data not found.";
            $result['statement_list'] = $t_result;
            $this->doRespond(204, $result);
        } else {
            
            $status = 'sent';
            foreach ($query_array as $row) {
                
                if ($row['from'] == $user_id) $status = 'sent';
                else $status = 'received';
                
                $row['status'] = $status;
                
                array_push($t_result, $row);
            }
            
            $result['statement_list'] = $t_result;            
            $this->doRespondSuccess($result);            
        }
        
        
    }
    
    function editStore() {
        
        $result = array();
        
        $id = $_POST['id'];
        $name = $_POST['name'];
        $description = $_POST['description']; 
        
        if (empty($_FILES['image'])) {
            
            $data = array('name' => $name,
                          'description' => $description,
                          );
            $this->api_model->editStore($id, $data);
            $this->doRespondSuccess($result);
        } else {            
            if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;

            // Upload file. 

            $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 3000,
                'max_height' => 3000,
                'file_name' => $name.'_'.intval(microtime(true) * 10)
            );

            $this->load->library('upload', $w_uploadConfig);

            if ($this->upload->do_upload('image')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array('name' => $name,
                              'description' => $description,
                              'image_url' => $file_url,
                              );
                $id = $this->api_model->editStore($id, $data);
                $this->doRespondSuccess($result);

            } else {

                $this->doRespond(206, $result);// upload fail
                return;
            }            
        }      
        
    }    
    
    function editProduct() {
        
        $result = array();
        
        $name = $_POST['name'];
        $description = $_POST['description'];
        $price = $_POST['price'];       
        $id = $_POST['id'];       

        if (empty($_FILES['image'])) {
            
            $data = array('name' => $name,
                          'description' => $description,
                          'price' => $price, 
                          );
            $this->api_model->editProduct($id, $data);
            $this->doRespondSuccess($result); 
            
        } else {
            
            if(!is_dir("uploadfiles/")) {
                mkdir("uploadfiles/");
            }
            $upload_path = "uploadfiles/";  

            $cur_time = time();
             
            $dateY = date("Y", $cur_time);
            $dateM = date("m", $cur_time);
             
            if(!is_dir($upload_path."/".$dateY)){
                mkdir($upload_path."/".$dateY);
            }
            if(!is_dir($upload_path."/".$dateY."/".$dateM)){
                mkdir($upload_path."/".$dateY."/".$dateM);
            }
             
            $upload_path .= $dateY."/".$dateM."/";
            $upload_url = base_url().$upload_path;

            // Upload file. 

            $w_uploadConfig = array(
                'upload_path' => $upload_path,
                'upload_url' => $upload_url,
                'allowed_types' => "*",
                'overwrite' => TRUE,
                'max_size' => "100000KB",
                'max_width' => 3000,
                'max_height' => 3000,
                'file_name' => $name.'_'.intval(microtime(true) * 10)
            );

            $this->load->library('upload', $w_uploadConfig);

            if ($this->upload->do_upload('image')) {

                $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
                $data = array('name' => $name,
                              'description' => $description,
                              'price' => $price, 
                              'image_url' => $file_url,     
                              );
                $this->api_model->editProduct($id, $data);                            
                $this->doRespondSuccess($result);

            } else {

                $this->doRespond(206, $result);// upload fail
                return;
            }
            
        }       
        
    }
                                     
    
}
?>
